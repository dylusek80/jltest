//
//  ProductGridList.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import Foundation

struct ProductGridList {
    let products : [ProductGridDetails]
    
    init(json: [String: Any]) {
        var productsList = [ProductGridDetails]()
        
        if let productsJson = json["products"] as? [[String : Any]] {
            for json in productsJson {
                let productGenericDetails = ProductGridDetails(json: json)
                productsList.append(productGenericDetails)
            }
        }
        
        self.products = productsList
    }
}
