//
//  ProductGridDetails.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import Foundation

struct ProductGridDetails {
    
    let productId: String
    let priceNow: String
    let title: String
    let imageURL : String
    
    init(json: [String: Any]) {
        productId = (json["productId"] as? String)!
        priceNow = ((json["price"] as? [String: Any])!["now"] as? String)!
        title = (json["title"] as? String)!
        imageURL = (json["image"] as? String)!
    }
}
