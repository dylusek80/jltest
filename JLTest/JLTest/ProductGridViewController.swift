//
//  ProductGridViewController.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import UIKit

class ProductGridViewController: UIViewController {

    @IBOutlet weak var productGridCollectionView: UICollectionView!
    var dataSource : ProductGridDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchProductGrid();
    }
    
    func fetchProductGrid() {
        let networkManager = NetworkManager()
        networkManager.getProductsList(pageSize: 20) { [weak self] (result:Result<ProductGridList>) in
            
            switch result {
            case .success(let productsGridList):
                    self?.dataSource = ProductGridDataSource()
                    self?.dataSource?.productsList = productsGridList
                    self?.productGridCollectionView.dataSource = self?.dataSource
                break
            default:
                break
                
            }
        }
    }
}
