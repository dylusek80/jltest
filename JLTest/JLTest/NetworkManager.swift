//
//  NetworkManager.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

enum NetworkManagerError: Error {
    case unknow
    case general
}

enum Result<T> {
    case success(T)
    case failure(Error)
}

class NetworkManager: NSObject {
    
    static let networkProtocol = "https:"
    private let apiUrl = "https://api.johnlewis.com/v1/products/"
    private let apiSearch = "search"
    private let query = "q"
    private let queryValue = "dishwasher"
    private let pageSizeParameter = "pageSize"
    private let key = "key"
    private let keyValue = "Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb"

    func getProductsList(pageSize: NSInteger, completion: @escaping (Result<ProductGridList>) -> Void ) {
        let productUrl = apiUrl + apiSearch
        let parameters : Parameters = [query: queryValue, key : keyValue, pageSizeParameter: String(pageSize)]
        
        Alamofire.request(productUrl, parameters: parameters).validate().responseJSON { response in
            
            switch response.result {
                
            case .failure(let error):
                return completion(.failure(error))
                
            case .success:
                guard let json = response.result.value as? [String:Any] else {
                    return completion(.failure(NetworkManagerError.general))
                }
                let productlist = ProductGridList(json: json)
                return completion(.success(productlist))
            }
        }
    }
    
    func getProductPage(productId: String, completion: @escaping (Result<ProductPage>) -> Void ) {
        let productUrl = apiUrl + productId
        let parameters : Parameters = [key: keyValue]
        
        Alamofire.request(productUrl, parameters: parameters).validate().responseJSON { response in
            
            switch response.result {
                
            case .failure(let error):
                return completion(.failure(error))
                
            case .success:
                guard let json = response.result.value as? [String:Any] else {
                    return completion(.failure(NetworkManagerError.general))
                }
                let productPage = ProductPage(json: json)
                return completion(.success(productPage))
            }
        }
    }
}
