//
//  ProductGridCell.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import UIKit

class ProductGridCell: UICollectionViewCell {
    
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    func setupView(product: ProductGridDetails) {
        productPrice.text = "£" +  product.priceNow
        productTitle.text = product.title
        
        let imageUrl = NetworkManager.networkProtocol + product.imageURL
        if let url = URL(string:imageUrl) {
            productImage.af_setImage(withURL: url)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImage?.af_cancelImageRequest()
        productImage?.image = nil
        productPrice.text = ""
        productTitle.text = ""
    }
}
