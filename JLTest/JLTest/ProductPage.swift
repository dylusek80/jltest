//
//  ProductPage.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import Foundation

struct ProductPage {
    
    let title: String
    let priceNow: String
    let productInformation: String
    let displaySpecialOffer: String
    let includedServices: [String]
    let code: String
    let attributesName: String
    let attributesValue: String
    let mediaURLs : [String]
    
    init(json: [String: Any]) {
        title = (json["title"] as? String)!
        priceNow = ((json["price"] as? [String: Any])!["now"] as? String)!
        productInformation = ((json["details"] as? [String: Any])!["productInformation"] as? String)!
        displaySpecialOffer = (json["displaySpecialOffer"] as? String)!
        includedServices = ((json["additionalServices"] as? [String: Any])!["includedServices"] as? [String])!
        code = (json["code"] as? String)!
        if let features = (json["details"] as? [String: Any])?["features"] as? [Any] {
            if let feature = features[0] as? [String: Any] {
                if let attributes = feature["attributes"] as? [Any] {
                    if let name = (attributes[0] as? [String:Any])?["name"] as? String {
                        self.attributesName = name
                    } else {
                        self.attributesName = ""
                    }
                    if let value = (attributes[0] as? [String:Any])?["value"] as? String {
                        self.attributesValue = value
                    } else {
                        self.attributesValue = ""
                    }
                } else {
                    self.attributesValue = ""
                    self.attributesName = ""
                }
            } else {
                self.attributesValue = ""
                self.attributesName = ""
            }
        } else {
            self.attributesValue = ""
            self.attributesName = ""
        }

        mediaURLs = (((json["media"] as? [String: Any])!["images"] as? [String: Any])!["urls"] as? [String])!
    }
}
