//
//  ProductGridDataSource.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import UIKit

class ProductGridDataSource: NSObject, UICollectionViewDataSource {
    
    var productsList : ProductGridList?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let productsList = productsList else {
            return 0
        }
        
        return productsList.products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductGridCell", for: indexPath) as? ProductGridCell else {
            fatalError("Couldn't find cell")
        }
        
        if let product = productsList?.products[indexPath.row] {
            cell.setupView(product: product)
        }
        
        return cell
    }
}
