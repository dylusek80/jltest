//
//  ProductGridDataSourceTests.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import XCTest
@testable import JLTest

class ProductGridDataSourceTests: XCTestCase {
    
    var dataSource: ProductGridDataSource?
    var collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var productGridJsonContent : [String: Any]?
    var productGridList: ProductGridList?

    
    override func setUp() {
        super.setUp()
        dataSource = ProductGridDataSource()
        
        let bundle = Bundle(for: type(of: self))
        if let file = bundle.url(forResource: "ProductGrid", withExtension: "json") {
            do {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    productGridJsonContent = object
                    productGridList = ProductGridList(json: object)
                } else {
                    XCTFail("Invalid JSON")
                }
            } catch {
                XCTFail(error.localizedDescription)
            }
        } else {
            XCTFail("Product Grid test file is missing")
        }
    }
    
    func testProductGridDataSourceReturnsRightNumberOfItemsForSection() {
        dataSource?.productsList = productGridList
        let numberOfItems = dataSource?.collectionView(collectionView, numberOfItemsInSection: 0)
        XCTAssertEqual(numberOfItems, productGridList?.products.count, "Wrong number of items for section 1")
    }
    
    func testProductGridDataSourceReturnsZeroItemsIfProductsNil()  {
        dataSource?.productsList = nil
        
        let numberOfItems = dataSource?.collectionView(collectionView, numberOfItemsInSection: 0)
        XCTAssertEqual(numberOfItems, 0, "Wrong number of items for section 1 when product are nil")
    }
}
