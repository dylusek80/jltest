//
//  ProductGridListTests.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import XCTest
@testable import JLTest

class ProductGridListTests: XCTestCase {
    
    var productGridJsonContent : [String: Any]?
    
    override func setUp() {
        super.setUp()
        
        let bundle = Bundle(for: type(of: self))
        if let file = bundle.url(forResource: "ProductGrid", withExtension: "json") {
            do {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    productGridJsonContent = object
                } else {
                    XCTFail("Invalid JSON")
                }
            } catch {
                XCTFail(error.localizedDescription)
            }
        } else {
            XCTFail("Product Grid test file is missing")
        }
     }
    
    override func tearDown() {
        productGridJsonContent = nil
        
        super.tearDown()
    }
    
    func testInitProductGridList() {
        if let json = productGridJsonContent {
            let productGridList = ProductGridList(json: json)
            XCTAssertEqual(productGridList.products.count, 20, "Wrong number of products")
        } else {
            
            XCTFail("No test data")
        }
    }
}
