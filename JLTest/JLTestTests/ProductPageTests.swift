//
//  ProductPageTests.swift
//  JLTest
//
//  Created by Michal on 02/04/2017.
//  Copyright © 2017 XPE Mobile. All rights reserved.
//

import XCTest
@testable import JLTest

class ProductPageTests: XCTestCase {
    
    var productPageJsonContent: [String: Any]?
    
    override func setUp() {
        super.setUp()
        
        let bundle = Bundle(for: type(of: self))
        if let file = bundle.url(forResource: "ProductPage", withExtension: "json") {
            do {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    productPageJsonContent = object
                } else {
                    XCTFail("Invalid JSON")
                }
            } catch {
                XCTFail(error.localizedDescription)
            }
        } else {
            XCTFail("Product Grid test file is missing")
        }
    }
    
    override func tearDown() {
        productPageJsonContent = nil
        super.tearDown()
    }
    
    func testInitProductPageExample() {
        if let productPageJson = productPageJsonContent {
            let productPage = ProductPage(json: productPageJson)
            XCTAssertEqual(productPage.title, "Bosch SMS53M02GB Freestanding Dishwasher, White", "Wrong product title")
            XCTAssertEqual(productPage.priceNow, "329.00", "Wrong product price")
            XCTAssertEqual(productPage.productInformation, "<p>The SMS53M02GB dishwasher offers a wide range of functions and enough space for 13 place settings to ensure easy cleaning of all your dishes, pots and pans. And with an impressive <strong>A++</strong> for energy efficiency, you can be sure this model is environmentally responsible and designed to save on running costs.</p>  <p><strong>Child lock</strong><br /> This clever design will ensure children can&rsquo;t tamper with the controls, giving you piece of mind when it comes to the safety of your home.</p>  <p><strong>VarioFlex basket system</strong><br /> This clever system is designed to cater to all of your cutlery, plates dishes, pots and pans. With top and bottom baskets, which are flexible in their design, you will have ample space no matter what needs cleaning.</p>  <p><strong>EcoSilence Drive&trade;</strong><br /> Because this machine is super quiet, you can take full advantage of off-peak energy costs without being disturbed during the night by noisy dishwashing. A brushless motor provides a quieter, faster, more energy efficient performance, while a timer delay of up to 24 hours lets you programme the dishwasher to start at a time most convenient for you.</p>  <p><strong>VarioSpeed</strong><br /> This function cleans dishes in up to half the time, whilst providing the best possible cleaning and drying results - great if you need your dishes in a hurry.</p>  <p><strong>ActiveWater hydraulic system</strong><br /> This dishwasher only uses 9.5 litres of water per wash, and every single drop is used to the maximum so you never have to worry about wasting any.</p>  <p><strong>AquaMix</strong><br /> This glass protection system ensures extra gentle handling for your delicate glasses, making sure they always come out sparkling and super clean without getting broken.</p>  <p><strong>Self-cleaning</strong><br /> The self-cleaning filter system with a 3-piece corrugated filter makes maintenance easier and will help to prolong the life and performance of your machine.</p>", "Wrong product information")
            XCTAssertEqual(productPage.displaySpecialOffer, "", "Wrong specila offer")
            XCTAssertEqual(productPage.includedServices.count, 1, "Wrong number of included services")
            XCTAssertEqual(productPage.includedServices[0], "2 year guarantee included", "Wrong first of included services")
            XCTAssertEqual(productPage.code, "81701209", "Wrong product code")
            XCTAssertEqual(productPage.attributesName, "Dimensions", "Wrong attribute name")
            XCTAssertEqual(productPage.attributesValue, "H84.5 x W60 x D62cm", "Wrong attribute value")
            XCTAssertEqual(productPage.mediaURLs.count, 5, "Wrong media urls count")
            XCTAssertEqual(productPage.mediaURLs[0], "//johnlewis.scene7.com/is/image/JohnLewis/234326367?", "Wrong first media url")
        } else {
            XCTFail("No test data")
        }
    }
    
}
